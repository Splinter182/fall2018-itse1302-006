package Ch4_conditionals;
import java.util.Scanner;

public class Conditionals {
	
	public static void main(String[] args)
	{
		String name1 = "Manuel";
		String name2 = "manuel";
		
		if( name1.compareTo(name2) < 0 )
		{
			System.out.println( name1 + " comes first");
		}
		else
		{
			if( name1.compareTo(name2) == 0 )
			{
				System.out.println( "same name");
			}
			else
			{
				System.out.println( name2 + " comes first");
			}
		}
		
		if( name1.compareTo(name2) < 0 )
		{
			System.out.println( name1 + " comes first");
		}
		else if( name1.compareTo(name2) == 0 )
		{
			System.out.println( "same name");
		}
		else
		{
			System.out.println( name2 + " comes first");
		}
		
		int grade = 60;
		
		if( grade >= 90 )
		{
			System.out.println("A");
		}
		else if( grade >= 80 )
		{
			System.out.println("B");
		}
		else if( grade >= 70 )
		{
			System.out.println("C");
		}
		else
		{
			System.out.println("D");
		}
		
		System.out.print("Enter day of the week: ");
		Scanner scan = new Scanner(System.in);
		String day = scan.nextLine();
		
		int number = 0;
		
		switch( day.toLowerCase() )
		{
		case "monday":
			number = 1;
			break;
		case "tuesday":
			number = 2;
			break;
		case "wednesday":
			number = 3;
			break;
		case "thursday":
			number = 4;
			break;
		case "friday":
			number = 5;
			break;
		case "saturday":
			number = 6;
			break;
		case "sunday":
			number = 7;
			break;
		}
		
		System.out.println("Number is: " + number);
	}

}
