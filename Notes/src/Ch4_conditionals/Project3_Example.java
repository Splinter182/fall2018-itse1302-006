package Ch4_conditionals;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

public class Project3_Example {
	
	public static void main(String[] args)
	{	
		String weekday;
		int weekdayNumber = 0;
		
		Scanner scan = new Scanner(System.in);
		
//		System.out.println("if-else approach");
//		System.out.print("Enter day of the week: ");
//		weekday = scan.nextLine();
//		
//		if( weekday.equalsIgnoreCase("monday") )
//		{
//			weekdayNumber = 1;
//		}
//		else if( weekday.equalsIgnoreCase("tuesday"))
//		{
//			weekdayNumber = 2;
//		}
//		else if( weekday.equalsIgnoreCase("wednesday"))
//		{
//			weekdayNumber = 3;
//		}
//		else if( weekday.equalsIgnoreCase("thursday"))
//		{
//			weekdayNumber = 4;
//		}
//		else if( weekday.equalsIgnoreCase("friday"))
//		{
//			weekdayNumber = 5;
//		}
//		else if( weekday.equalsIgnoreCase("saturday"))
//		{
//			weekdayNumber = 6;
//		}
//		else if( weekday.equalsIgnoreCase("sunday"))
//		{
//			weekdayNumber = 7;
//		}
//		else
//		{
//			weekdayNumber = -1;
//		}
//		
//		System.out.println("Your weekday number: " + weekdayNumber);
//		
//		System.out.println("\nswitch approach");
//		System.out.print("Enter day of the week: ");
//		weekday = scan.nextLine();
//		
//		switch( weekday.toLowerCase() )
//		{
//		case "monday":
//			weekdayNumber = 1;
//			break;
//		case "tuesday":
//			weekdayNumber = 2;
//			break;
//		case "wednesday":
//			weekdayNumber = 3;
//			break;
//		case "thursday":
//			weekdayNumber = 4;
//			break;
//		case "friday":
//			weekdayNumber = 5;
//			break;
//		case "saturday":
//			weekdayNumber = 6;
//			break;
//		case "sunday":
//			weekdayNumber = 7;
//			break;
//		default:
//			weekdayNumber = -1;
//			break;
//		}
//		
//		System.out.println("Your weekday number: " + weekdayNumber);
//		
//		System.out.println("\nArray approach");
//		final String[] DAYS = {"monday","tuesday","wednesday","thursday","friday","saturday","sunday"};
//
//		System.out.print("Enter day of the week: ");
//		weekday = scan.nextLine();
//		weekdayNumber = Arrays.asList(DAYS).indexOf(weekday.toLowerCase()) + 1;
//		System.out.println("Your weekday number: " + weekdayNumber);
		
		int first;
		int second;
		int difference;
		System.out.print("first number: ");
		first = scan.nextInt();
		System.out.print("second number: ");
		second = scan.nextInt();
		
		if( first > second )
		{
			difference = first - second;
		}
		else //second > first and first == second
		{
			difference = second - first;
		}
		
		//year1,month1
		//year2,month2
		
		//if year1 > your2
		//year1 - year2
		//month1 - month2
		
		System.out.print("The difference is: " + difference);
	}
}
