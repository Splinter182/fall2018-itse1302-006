package Ch5_loops;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Loops {

	public static void main(String[] args) {
		
		int count1 = 1;
		
		while (count1 <= 10)
		{
			int count2 = 1;
			while (count2 <= 20)
			{
//				System.out.println( count2 + " Here");
				count2++;
			}
			count1++;
		}
		
		System.out.println("while");
		int count3 = 1;//initialization
		while (count3 <= 5){//condition
			System.out.println(count3);
			count3++;//increment
		}

		while( getInput() )
		{
			
		}
		
		System.out.println("do-while");
		int count = 0;//initialization
		do{
			count++;//increment
			System.out.println(count);
		}while(count < 5);//condition
		
		System.out.println("for");
		//for( initialization ; condition ; increment )
		for (int count4 = 1; count4 <= 5; count4++){
			System.out.println(count4);
		}
		
		//infinite for loop
		for( ; ; )
		{
			break;
		}
		
		boolean exit = false;
		//infinite while loop
		while ( true )
		{
			count++;
			if( exit )
			{
				break;
			}
			count--;
		}
		
		System.out.println();
		System.out.println("for");
		String[] numbers = {"zero","one","two","three","four","five","six","seven","eight","nine"};
		for(int ndx = 0 ; ndx < numbers.length/*10*/ ; ndx++)
		{
			System.out.println(numbers[ndx]);
		}
		
		System.out.println();
		System.out.println("while");
		int ndx = 0;
		while(ndx < numbers.length)
		{
			System.out.println(numbers[ndx]);
			ndx++;
		}
		
		System.out.println();
		System.out.println("foreach");
		for( String current : numbers )
		{
			System.out.println(current);
		}

	}
	
	public static boolean getInput() {
		return true;
	}

}
