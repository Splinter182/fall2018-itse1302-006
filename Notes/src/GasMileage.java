
import java.util.Scanner;

/**
 * Calculates fuel efficiency based on values entered by  the user.
 * @author Lewis Loftus
 * @version 1.0
 */
public class GasMileage{
   /**
    * Calculates fuel efficiency based on values entered by the user.
    * @param args A reference to a string array containing command-line arguments
    */
   public static void main (String[] args){
      int miles;
      double gallons, mpg;

      Scanner scan = new Scanner (System.in);

      System.out.print ("Enter the number of miles: ");
      miles = scan.nextInt();

      System.out.print ("Enter the gallons of fuel used: ");
      gallons = scan.nextDouble();

      mpg = miles / gallons;

      System.out.println ("Miles Per Gallon: " + mpg);
   }
}

