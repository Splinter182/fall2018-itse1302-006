import java.awt.datatransfer.StringSelection;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Scanner;

public class InputEx {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Input 2 strings followed by 3 numbers
		
		Scanner scan = new Scanner(System.in);
		String fname;
		String lname;
		int n1;
		int n2;
		double price;
		
		//inputStream = "13\n"
		
//		fname = scan.nextLine();
//		lname = scan.nextLine();
//		n1 = Integer.parseInt( scan.nextLine() ); //"13\n"
//		scan.nextLine();
//		price = Double.parseDouble( scan.nextLine() );
		
//		System.out.print("Enter a number: ");
////		n1 = scan.nextInt();
//		n1 = Integer.parseInt( scan.nextLine() );
//		System.out.println("Enter first name: ");
//		fname = scan.nextLine();
//		System.out.println("Enter last name: ");
//		lname = scan.nextLine();
		
//		System.out.println(n1);
//		System.out.println(fname);
//		System.out.println(lname);
		
		double d1 = 14.20;
		DecimalFormat doubleFormat = new DecimalFormat(".00");
		System.out.println( d1 );
		System.out.println( doubleFormat.format(d1) );
		
		int length = 10;
		DecimalFormat digitFormat3 = new DecimalFormat("000");
		System.out.println( length );
		System.out.println( digitFormat3.format(length) );
		
		DecimalFormat currency = new DecimalFormat("0000.00");
		
		double total = 200.99;
		System.out.println(total);
		System.out.println( " " + currency.format(total));
		System.out.println( "$" + currency.format(total) );
		
		final double TAX_RATE = 0.085;
		NumberFormat percent1 = NumberFormat.getPercentInstance();
		DecimalFormat percent = new DecimalFormat(".00%");
		System.out.println( TAX_RATE );
		System.out.println( percent1.format(TAX_RATE) );
		System.out.println( percent.format(TAX_RATE));
		
		System.out.println("CHARGES");
		System.out.println("Description" + "\t" + "Cost/Sq.Ft." + "\t" + "Charges");
		System.out.println("-----------" + "\t" + "-----------" + "\t" + "-------");
		System.out.println();

	}

}
