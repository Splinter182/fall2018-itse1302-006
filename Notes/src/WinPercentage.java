import java.text.NumberFormat;
import java.util.Scanner;

/**
 * Computes the percentage of won by a team.
 * @author  Lewis Loftus
 * @version 1.0
 */
public class WinPercentage{
	/**
	 * Computes the percentage of games won by a team.
	 * @param args A reference to a string array containing command-line arguments
	 */
	public static void main (String[] args){
		final int NUM_GAMES = 12;
		int won;
		double ratio;
		Scanner scan = new Scanner (System.in);
		NumberFormat fmt = NumberFormat.getPercentInstance();

		System.out.print ("Enter the number of games won (0 to "+ NUM_GAMES + "): ");
		won = scan.nextInt();

		while (won < 0 || won > NUM_GAMES){
			System.out.print ("Invalid input. Please reenter: ");
			won = scan.nextInt();
		}

		ratio = (double)won / NUM_GAMES;

		System.out.println ();
		System.out.println ("Winning percentage: " + fmt.format(ratio));
	}
}

