package classes;

import java.util.Scanner;

public class Arrays {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		final int NUMBER_OF_STUDENTS = 4;
		
		int[] score1 = new int[NUMBER_OF_STUDENTS];
		int[] score2 = new int[NUMBER_OF_STUDENTS];
		
//		score1[0] = scan.nextInt();
//		score1[1] = scan.nextInt();
//		score1[2] = scan.nextInt();
//		score1[3] = scan.nextInt();
	
		for( int i = 0 ; i < score1.length ; i++ )
		{
			System.out.println( "Enter first score for student " + (i+1) );
			score1[i] = scan.nextInt();
		}
		
		for( int i = 0 ; i < score2.length ; i++ )
		{
			System.out.println( "Enter second score for student " + (i+1) );
			score2[i] = scan.nextInt();
		}
		
		
	}
}
