package classes;

import java.util.Random;

public class Dog {
	
	private String name;
	private int age;
	private String breed;
	private String color;
	
	public Dog() {
		name = "Spot";
		age = 1;
		breed = "Corgie";
		color = "Tan";
	}
	
	public Dog( String name, int age, String breed, String color ) {
		this.name = name;
		this.age = age;
		this.breed = breed;
		this.color = color;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName( String name ){
		this.name = name;
	}
	
	public String speak() {
		Random rand = new Random();
		int count = rand.nextInt(5) + 1;
		String ret = "";
		for(int i = 0 ; i < count ; i++) {
			ret += "Bork! ";
		}
		return ret;
	}
	
	public String toString() {
		return "Dog: Name: " + this.name 
				+ " Age: " + this.age 
				+ " Breed: " + this.breed 
				+ " Color: " + this.color;
	}
	
	public boolean equals( Object obj ) {
		if( obj == null )
		{
			return false;
		}
		if( !(obj instanceof Dog) )
		{
			return false;
		}
		Dog otherDog = (Dog)obj;
		return this.name.equals(otherDog.name) &&
				this.age == otherDog.age &&
				this.breed.equals(otherDog.breed) &&
				this.color.equals(otherDog.color);
	}
}
