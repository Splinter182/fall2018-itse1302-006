package classes;

public class DogTest {

	public static void main(String[] args)
	{
		//Test default contructor
		Dog d1 = new Dog();
		//Test getName
		System.out.println(d1.getName());
		
		//Test custom constructor
		Dog d2 = new Dog( "Fluffy", 2, "Pomeranian", "White" );
		//Test getName
		System.out.println(d2.getName());
		
		//Test custom constructor
		Dog d3 = new Dog( "Rick James", 3, "Pit Bull", "Black");
		//Test getName
		System.out.println(d3.getName());
		
		//Test setName
		d1.setName( "Achilles" );
		System.out.println(d1.getName());
		
		//Test speak
		System.out.println(d1.speak());
		
		//Test toString
		System.out.println( d1.toString() );
		
		//Test equals method
		Dog d4 = new Dog( "Rick James", 3, "Pit Bull", "Black");
		System.out.println(d3.equals(d4));
		
	}
}
