package notes;

import java.util.Scanner;
import java.text.DecimalFormat;

/**
 * Calculates the area and the circumference of a circle.
 * @author  Lewis Loftus
 * @version 1.0
 */
public class CircleStats{
   /**
    * Calculates the area and circumference of a circle given its radius.
    * @param args A string array containing command-line arguments.
    */
   public static void main (String[] args){
      int radius;
      double area, circumference;
      Scanner scan = new Scanner (System.in);
      
      //Ask the user to enter a radius.
      System.out.print ("Enter the circle's radius: ");
      radius = scan.nextInt();
      
      //Calculate the area and the circumference.
      area = Math.PI * Math.pow(radius, 2);
      circumference = 2 * Math.PI * radius;
      
      System.out.println(area);
      System.out.println(circumference);

      // Round the output to three decimal places
      DecimalFormat fmt = new DecimalFormat ("0.###");
      System.out.println ("The circle's area: " + fmt.format(area));
      System.out.println ("The circle's circumference: " + fmt.format(circumference));
      
      Integer i = new Integer(0);
      
      String str = "3.0";
      double n = Double.parseDouble(str);
      System.out.println(n);
   }
}

