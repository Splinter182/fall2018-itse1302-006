package notes;

import java.util.Scanner;
import java.util.stream.StreamSupport;

import javax.swing.plaf.synth.SynthSeparatorUI;

public class HelloWorld
{
	public static void main(String[] args)
	{
		final double TAX_RATE = 0.0825;
		final double PI = 3.1415;
		
		Scanner scan = new Scanner(System.in);
//		
//		System.out.print("Enter a number: ");
//		int number = scan.nextInt();
//		
//		System.out.println( "My number is:\t" + (number + 1) );
//		System.out.printf( "My number is:\n%d", number);
		
//		int resultOfTheDivisionOperation = 14/3;
//		int remainder = 14%3;
//		System.out.println( "result: " + resultOfTheDivisionOperation 
//				+ "\nRemainder: " + remainder );
		
		
//		double resultWithDecimal = 14.0/3;
//		System.out.println( "Result with decimal: " + resultWithDecimal );
		
//		System.out.print("first: ");
//		int first = scan.nextInt();
//		System.out.print("second: ");
//		int second = scan.nextInt();
//		
//		double result = (double)first/second;
//		System.out.println( "Result: " + result );
//		System.out.println( (int)result );
		
		int n1;
		n1 = 2;
		int n2 = 3;
		
		boolean isTrue = true;
		byte aByte;
		short aShort;
		int anInt;
		
		float aFloat;
		double aDouble;
		
		int x = 1;
		x = x + 1;
		x++;
		x = x - 1;
		x--;
		
		x+=10;
		x = x + 10;
		System.out.println(x);
		
		
		int y = x++;
		System.out.println( "x = " + x); //2
		System.out.println( "y = " + y); //1
		y = ++x;
		System.out.println( "x = " + x);
		System.out.println( "y = " + y);
	}
}