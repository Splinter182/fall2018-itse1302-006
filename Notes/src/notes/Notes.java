package notes;

public class Notes {

	public static void main(String[] args) {
		
		String str1 = "smith";
		String str2 = "Smith";
		String str3 = "smith";
		String str4 = str1.substring(0, 1).concat(str2.substring(1));
		
		System.out.println( Integer.toHexString(str1.hashCode() ));
		System.out.println( Integer.toHexString(str2.hashCode() ));
		System.out.println( str1 == str2 );
		System.out.println( str1.equals(str2) );
		System.out.println( str1.equalsIgnoreCase(str2) );
		System.out.println( Integer.toHexString(str3.hashCode() ));
		System.out.println( Integer.toHexString(str4.hashCode() ));
		
		String str5 = "abA";
		String str6 = "aba";
		
		System.out.println( str5.compareTo(str6) );
		
		Cookie c1 = new Cookie();
		Cookie c2 = new Cookie();
		
		System.out.println("c1 = " + c1.myNumber);
		System.out.println("c2 = " + c2.myNumber);
		System.out.println("Cookie count = " + Cookie.count);
	}

}
