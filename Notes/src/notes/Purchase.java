package notes;

import java.util.Scanner;
import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * Calculates the total cost of a purchase.
 * @author  Lewis Loftus
 * @version 1.0
 */
public class Purchase{
   /**
    * Calculates the final price of a purchased item using 
    * values entered by the user.
    * @param args A string array containing command-line arguments.
    */
   public static void main (String[] args){
      final double TAX_RATE = 0.06; 
      int quantity;
      double subtotal, tax, totalCost, unitPrice;
      Scanner scan = new Scanner (System.in);
      NumberFormat fmt1 = NumberFormat.getCurrencyInstance();
      NumberFormat fmt2 = NumberFormat.getPercentInstance();

      //Get the quantity and the unite price from the user.
      System.out.print ("Enter the quantity: ");
      quantity = scan.nextInt();

      System.out.print ("Enter the unit price: ");
      unitPrice = scan.nextDouble();      
      
      //Calculate the total cost
      subtotal = quantity * unitPrice;
      tax = subtotal * TAX_RATE;
      totalCost = subtotal + tax;

      System.out.println( subtotal );
      System.out.println( tax );
      System.out.println( totalCost );
      
      // Print output with appropriate formatting
      System.out.println ("Subtotal: " + fmt1.format(subtotal));
      System.out.println ("Tax: " + fmt1.format(tax) + " at "
                          + fmt2.format(TAX_RATE));
      System.out.println ("Total: " + fmt1.format(totalCost));
   }
}


      
